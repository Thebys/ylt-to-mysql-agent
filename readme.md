This application queries YLT public API and parses resulting JSON data to MYSQL.

It requires the following:

# PHP, Composer and dependencies
Use composer install to install everything.
* Guzzle        - https://docs.guzzlephp.org/en/stable/overview.html
* PHP .ENV      - https://github.com/vlucas/phpdotenv
# Mysql
MYSQL is used to store the resulting data (to be accessed by Grafana).
* server
* database
* user
* password
# Log file
Define in .ENV file to see how the script works and examine execution or errors.

# TODOs
- We could use some interface
- Pruning of downloaded JSON result files would be cool, also
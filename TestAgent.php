<?php

require 'vendor/autoload.php';

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
try {
    $dotenv->Load();
} catch (Exception $e) {
    logMessage($e->getMessage());
}

$db = new stdClass;
$db->servername = $_ENV['MYSQL_SERVER'];
$db->username = $_ENV['MYSQL_USER'];
$db->password = $_ENV['MYSQL_PW'];
$db->dbname = $_ENV['MYSQL_DB'];

/* start from CLI or CRON */
logMessage("Test run initiated at " . (new DateTime())->format("d. m. Y - h:i:s T"));
testAllTargets($db);

/* definitions */
function testAllTargets($db)
{
    //get targets from DB
    try {
        $conn = new PDO("mysql:host=$db->servername;dbname=$db->dbname", $db->username, $db->password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $sql = "SELECT * FROM targets WHERE lastRun < DATE(NOW() - INTERVAL 20 HOUR) OR (lastResult IS NULL)";
        $targets = $conn->query($sql)->fetchAll(PDO::FETCH_OBJ);
        logMessage("Found " . count((array) ($targets)) . " targets to test.");
        //execute test run for all targets, selected by above query.
        foreach ($targets as $target) {
            testWebPage($db, $target->url);
        }
        logMessage("Test run has been succesfully finished, enjoy.");

    } catch (PDOException $e) {
        logMessage("Test run has failed.\r\n" . $e->getMessage());
    }
}
function testWebPage($db, $targetURL)
{
    $client = new Client([
        //Documentation for YLT RESTful API:    https://github.com/YellowLabTools/YellowLabTools/wiki/Public-API
        // Base URI is used with relative requests
        'base_uri' => 'https://yellowlab.tools/api/',
        // You can set any number of default request options.
        'timeout' => 30.0,
        'headers' => ['Content-Type' => 'application/json'],
    ]);
    try {
        $resultFile = parseResult(orderTestDownloadResult($client, $targetURL));
        saveResultToMysql($db, $targetURL, $resultFile);
    } catch (Error $e) {
        logMessage("Test run failed for target " . $targetURL . " with " . $e->getMessage());
    }
}
function orderTestDownloadResult($client, $targetURL)
{
    $runStatus = 'not ordered';
    $runId = orderNewTest($client, $targetURL);
    $resultFilePath = "";

    while ($runStatus != 'complete' || $runStatus != 'failed') {
        sleep(2); //debug anti-flood.
        $runStatus = checkTestRunStatus($client, $runId);
        switch ($runStatus) {
            case "awaiting":
                sleep(20);
                break 1;
            case "running":
                sleep(10);
                break 1;
            case "failed":
                logMessage("Test run id " . $runId . " has failed!");
                break 2;
            case "complete":
                logMessage("Test run id " . $runId . " has finished.");
                $resultFilePath = retrieveResult($client, $runId, $targetURL);
                break 2;
        }
    }
    return $resultFilePath;
}
function orderNewTest($client, $targetURL)
{
    //Prepare a test order request with required parameters and target url (of page we want to test).
    $target = [
        'json' => [
            "url" => $targetURL,
            "waitForResponse" => false,
        ],
    ];

    //send the test order request.
    $response = $client->post('runs', $target);

    //if test has been ordered successfully return runId, otherwise fail with error.
    $responseCode = $response->getStatusCode();
    if ($responseCode == 200) {
        $responseJSON = json_decode($response->getBody());
        logMessage('YLT test for ' . $targetURL . ' ordered succefully. RunId is: ' . $responseJSON->runId . ".");
        return $responseJSON->runId;
    } else {
        logMessage('Ordering new test failed with code: ', $responseCode, ' .');
    }
}
function checkTestRunStatus($client, $runId)
{
    //send the test order request.
    $testStatusRequest = new Request('GET', "runs/" . $runId);
    $response = $client->send($testStatusRequest);

    //return test run status awaiting/running/complete/failed
    $responseCode = $response->getStatusCode();
    if ($responseCode == 200) {
        $responseJSON = json_decode($response->getBody());
        $testRunStatus = $responseJSON->status->statusCode;
        if (isset($responseJSON->status->error)) {
            logMessage('Test run ' . $runId . ' failed with error: ' . $responseJSON->status->error);
            return "failed";
        }
        logMessage('Test run ' . $runId . " status is: " . $testRunStatus . '.');
        return $testRunStatus;
    }
}
function retrieveResult($client, $runId, $targetURL)
{
    $resultFileName = parse_url($targetURL, PHP_URL_HOST) . "-" . date("Ymd-Hi") . ".json";
    $resultFilePath = __DIR__ . "/data/fresh/" . $resultFileName;
    logMessage("Retrieving test result of " . $targetURL . " test run id " . $runId . " to file: " . $resultFilePath . ".");

    //GET https://yellowlab.tools/api/results/<runId>/rules
    //send the test result request.
    $testResultRequest = new Request('GET', "results/" . $runId . '/rules');
    $response = $client->send($testResultRequest);

    //return downloaded .json file path if ok.
    //add error 404 handling...
    $responseCode = $response->getStatusCode();
    if ($responseCode == 200) {
        $response = $response->getBody();

        $resultFile = fopen($resultFilePath, 'w');
        fwrite($resultFile, $response);
        fclose($resultFile);

        if (filesize($resultFilePath) > 1) {
            logMessage(formatSizeUnits(filesize($resultFilePath)) . " JSON test result file has been saved successfully.");
            return $resultFilePath;
        } else {
            throw new Exception("JSON Test result file retrieval/saving has failed.\r\n" . $response);
        }
    }
}
function parseResult($resultFile)
{
    //This will take freshly downloaded .json file and parse the most interesting values.
    $JSON = json_decode(file_get_contents($resultFile), true);
    $ignoredKeys = ["jQueryVersion"];
    $ResultArray = [];
    foreach ($JSON as $key => $valueArray) {
        if (!in_array($key, $ignoredKeys)) {
            if (isset($valueArray['value'])) {
                $ResultArray[$key] = $valueArray['value'];
            }
        }
    }
    return $ResultArray;
}
function createMysqlResultsTable($db, $resultArray)
{
    $resultColumnDefinitions = '';
    foreach ($resultArray as $key => $valueArray) {
        $resultColumnDefinitions .= $key . " INT UNSIGNED,";
    }
    $resultColumnDefinitions = substr($resultColumnDefinitions, 0, -1); //strip last comma.

    try {
        $conn = new PDO("mysql:host=$db->servername;dbname=$db->dbname", $db->username, $db->password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $sql = "CREATE TABLE testresults
        (id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        target_url VARCHAR(128) NOT NULL,
        test_date_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        " . $resultColumnDefinitions . ")";

        $conn->exec($sql);
    } catch (PDOException $e) {
        echo $sql . "\r\n" . $e->getMessage();
    }

    $conn = null;
}
function saveResultToMysql($db, $targetURL, $resultArray)
{
    $resultKeys = 'target_url,';
    foreach ($resultArray as $key => $value) {
        $resultKeys .= $key . ",";
    }
    $resultKeys = substr($resultKeys, 0, -1); //strip last comma.

    $resultValues = '\'' . $targetURL . '\',';
    foreach ($resultArray as $value) {
        $resultValues .= $value . ",";
    }
    $resultValues = substr($resultValues, 0, -1); //strip last comma.

    try {
        $conn = new PDO("mysql:host=$db->servername;dbname=$db->dbname", $db->username, $db->password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        //insert new test result...
        $sql = "INSERT INTO testresults (" . $resultKeys . ") VALUES (" . $resultValues . ")";
        $conn->exec($sql);
        //and update targets table...
        $sql = "UPDATE targets SET lastRun=?, lastResult=? WHERE url=?";
        $conn->prepare($sql)->execute([(new DateTime())->format("Y-m-d h:i:s"), "OK", $targetURL]);

        logMessage("Result saved to database and target info updated.");

    } catch (PDOException $e) {
        //something is wrong, maybe result data strcuture changed?
        //update targets table at least...
        $conn = new PDO("mysql:host=$db->servername;dbname=$db->dbname", $db->username, $db->password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "UPDATE targets SET lastRun=?, lastResult=? WHERE url=?";
        $conn->prepare($sql)->execute([(new DateTime())->format("Y-m-d h:i:s"), "Failed", $targetURL]);

        logMessage("SQL Insert failed with error.\r\n" . $e->getMessage());
        throw new Exception("SQL Update for target failed.");
    }

    $conn = null;
}
function logMessage($message)
{
    $timestamp = date('Ymd-his T');
    $message = $timestamp . ": " . $message . "\r\n";

    echo ($message);
    file_put_contents($_ENV['LOG_FILE'], $message, FILE_APPEND);
}
function formatSizeUnits($bytes)
{
    if ($bytes >= 1073741824) {
        $bytes = number_format($bytes / 1073741824, 2) . ' GB';
    } elseif ($bytes >= 1048576) {
        $bytes = number_format($bytes / 1048576, 2) . ' MB';
    } elseif ($bytes >= 1024) {
        $bytes = number_format($bytes / 1024, 2) . ' KB';
    } elseif ($bytes > 1) {
        $bytes = $bytes . ' bytes';
    } elseif ($bytes == 1) {
        $bytes = $bytes . ' byte';
    } else {
        $bytes = '0 bytes';
    }

    return $bytes;
}
